import unittest
from parse import parse_austrian_plate, parse_german_plate, parse_belgian_plate, parse_czechia_plate, parse_danish_plate, parse_french_plate, parse_luxembourg_plate, parse_dutch_plate, parse_polish_plates, parse_swiss_plate

class TestGermanNumberPlateParsing(unittest.TestCase):

    def test6digits(self):
        self.assertEqual(parse_german_plate("MPP123"), "DE")
        self.assertEqual(parse_german_plate("APP123"), None)

    def test7digits(self):
        self.assertEqual(parse_german_plate("MNUP123"), "DE")
        self.assertEqual(parse_german_plate("AAPP123"), None)

    def test8digits(self):
        self.assertEqual(parse_german_plate("MZGUP123"), "DE")
        self.assertEqual(parse_german_plate("AAAPP123"), None)

class TestAustrianNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_austrian_plate("MPP123"), "AU")
        self.assertEqual(parse_austrian_plate("APP"), None)

class TestBelgianNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_belgian_plate("123ALL"), "BE")
        self.assertEqual(parse_belgian_plate("APP"), None)

class TestCzechiaNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_czechia_plate("1A12345"), "CZ")
        self.assertEqual(parse_czechia_plate("APP"), None)

class TestDanishNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_danish_plate("AB12345"), "DK")
        self.assertEqual(parse_danish_plate("APP"), None)

class TestFrenchNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_french_plate("AB213AA"), "FR")
        self.assertEqual(parse_french_plate("APP"), None)

class TestLuxembourgNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_luxembourg_plate("AA1234"), "LU")
        self.assertEqual(parse_luxembourg_plate("APP"), None)

class TestDutchNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_dutch_plate("12ABC3"), "NL")
        self.assertEqual(parse_dutch_plate("APP"), None)

class TestPolishNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_polish_plates("AB12345"), "PL")
        self.assertEqual(parse_polish_plates("APP"), None)

class TestSwissNumberPlateParsing(unittest.TestCase):

    def test(self):
        self.assertEqual(parse_swiss_plate("AA234567"), "CH")
        self.assertEqual(parse_swiss_plate("APP"), None)

# 9Switzerland(done)


if __name__ == '__main__':
    unittest.main()