import re
from app import app

# German number plate parsing
# REGION (1-3) - (5 RANDOM)

# German city codes: https://www.europeanplates.com/information/german-city-codes.html
germanRegions = ["AB","ABG","AC","AH","AIB","AIC","AE","AK","AL","ALF","ALS",
"ALZ","AM","AN","ANA","ANG","ANK","AÖ","AP","APD","AR","ARN","ART","AS","ASD",
"ASL","ASZ","AT","AU","AUR","AW","AZ","AZE","B","BA","BAD","BAR","BB","BBG",
"BBL","BC","BCH","BD","BE","BED","BEI","BEL","BER","BF","BG","BGD","BGL","BH",
"BI","BID","BIN","BIR","BIT","BIW","BK","BKS","BL","BLB","BLK","BM","BN","BNA",
"BO","BÖ","BOG","BOH","BOR","BOT","BP","BR","BRA","BRB","BRG","BRI","BRK","BRL",
"BRV","BS","BSB","BSK","BT","BTF","BU","BÜD","BUL","BUR","BÜS","BÜZ","BW","BWL",
"BYL","BZ","BZA","C","CA","CAS","CB","CE","CHA","CLP","CLZ","CO","COC","COE",
"CR","CUX","CW","D","DA","DAH","DAN","DAU","DB","DBR","DD","DE","DEG","DEL",
"DGF","DH","DI","DIL","DIN","DIZ","DKB","DL","DLG","DM","DN","DO","DON","DS",
"DT","DU","DUD","DÜW","DW","DZ","E","EA","EB","EBE","EBN","EBS","ECK","ED",
"EE","EF","EG","EH","EHI","EI","EIC","EIH","EIL","EIN","EL","EM","EMD","EMS",
"EN","ER","ERB","ERH","ERK","ES","ESA","ESB","ESW","EU","EUT","EW","F","FAL",
"FB","FD","FDB","FDS","FEU","FF","FFB","FG","FH","FI","FKB","FL","FLÖ","FN",
"FO","FOR","FR","FRG","FRI","FRW","FS","FT","FTL","FÜ","FÜS","FW","FZ","G",
"GA","GAN","GAP","GC","GD","GDB","GE","GEL","GEM","GEO","GER","GF","GG","GHA",
"GHC","GI","GK","GL","GLA","GM","GMN","GN","GNT","GÖ","GOA","GOH","GP","GR",
"GRA","GRI","GRM","GRS","GRZ","GS","GT","GTH","GÜ","GUB","GUN","GV","GVM","GW",
"GZ","H","HA","HAB","HAL","HAM","HAS","HB","HBN","HBS","HC","HCH","HD","HDH",
"HDL","HE","HEB","HEF","HEI","HEL","HER","HET","HF","HG","HGN","HGW","HH","HHM",
"HI","HIG","HIP","HL","HM","HMU","HN","HO","HOG","HOH","HOL","HOM","HOR","HÖS",
"HOT","HP","HR","HRO","HS","HSK","HST","HU","HÜN","HUS","HV","HVL","HW","HWI",
"HX","HY","HZ","IGB","IK","IL","ILL","IN","IS","IZ","J","JB","JE","JEV",
"JL","JÜL","K","KA","KAR","KB","KC","KE","KEH","KEL","KEM","KF","KG","KH",
"KI","KIB","KK","KL","KLE","KLT","KLZ","KM","KN","KO","KÖN","KÖT","KR","KRU",
"KS","KT","KU","KÜN","KUS","KW","KY","KYF","L","LA","LAN","LAT","LAU","LB",
"LBS","LBZ","LC","LD","LDK","LDS","LE","LEO","LER","LEV","LF","LG","LH","LI",
"LIB","LIF","LIN","LIP","LK","LL","LM","LN","LÖ","LÖB","LOH","LOS","LP","LR",
"LS","LSA","LSN","LSZ","LU","LÜD","LUK","LÜN","LWL","M","MA","MAB","MAI","MAK",
"MAL","MAR","MB","MC","MD","ME","MED","MEG","MEI","MEK","MEL","MEP","MER","MES",
"MET","MG","MGH","MGN","MH","MHL","MI","MIL","MK","ML","MM","MN","MO","MOD",
"MOL","MON","MOS","MQ","MR","MS","MSP","MST","MT","MTK","MTL","MÜ","MÜB","MÜL",
"MÜN","MÜR","MVL","MW","MY","MYK","MZ","MZG","N","NAB","NAI","NAU","NB","ND",
"NDH","NE","NEA","NEB","NEC","NEN","NES","NEU","NEW","NF","NH","NI","NIB","NK",
"NL","NM","NMB","NMS","NÖ","NOH","NOL","NOM","NOR","NP","NR","NRÜ","NRW","NU",
"NT","NVP","NW","NWM","NY","NZ","O","OA","OAL","OB","OBB","OBG","OC","OCH",
"OD","OE","OF","OG","OH","OHA","ÖHR","OHV","OHZ","OK","OL","OLD","OP","OPR",
"OR","OS","OSL","OTT","OTW","OVI","OVL","OVP","OZ","P","PA","PAF","PAN","PAR",
"PB","PCH","PE","PEG","PER","PF","PI","PIR","PK","PL","PLÖ","PM","PN","PR",
"PRÜ","PS","PW","PZ","QFT","QLB","R","RA","RC","RD","RDG","RE","REG","REH",
"REI","RG","RH","RI","RID","RIE","RL","RM","RN","RO","ROD","ROF","ROH","ROK",
"ROL","ROS","ROT","ROW","RPL","RS","RSL","RT","RÜD","RÜG","RV","RW","RWL","RY",
"RZ","S","SAB","SAD","SÄK","SAL","SAN","SAW","SB","SBG","SBK","SC","SCZ","SDH",
"SDL","SDT","SE","SEB","SEE","SEF","SEL","SF","SFA","SFB","SFT","SG","SGH","SH",
"SHA","SHG","SHK","SHL","SI","SIG","SIM","SK","SL","SLE","SLF","SLG","SLN","SLS",
"SLÜ","SLZ","SM","SMÜ","SN","SNH","SO","SOB","SOG","SOK","SOL","SÖM","SON","SP",
"SPB","SPN","SPR","SR","SRB","SRO","ST","STA","STB","STD","STE","STH","STL","STO",
"SU","SUL","SÜW","SW","SWA","SY","SZ","SZB","TBB","TE","TF","TG","THL","THW",
"TIR","TO","TÖL","TÖN","TP","TR","TS","TT","TÜ","TUT","ÜB","UE","UEM","UER",
"UFF","UH","UL","UM","UN","USI","V","VAI","VB","VEC","VER","VIB","VIE","VIT",
"VK","VL","VOF","VOH","VS","W","WA","WAF","WAK","WAN","WAR","WAT","WB","WBS",
"WD","WDA","WE","WEB","WEG","WEL","WEM","WEN","WER","WES","WF","WG","WHV","WI",
"WIL","WIS","WIT","WIZ","WK","WL","WLG","WM","WMS","WN","WND","WO","WOB","WOH",
"WOL","WOR","WOS","WR","WRN","WS","WSF","WST","WSW","WT","WTL","WTM","WÜ","WUG",
"WUM","WUN","WUR","WW","WZ","WZL","X","Y","Z","ZE","ZEL","ZI","ZIG","ZP",
"ZR","ZS","ZW","ZZ"]


# TODO: We could use a binary tree on the regions to make the parsing much quicker
def parse_german_plate(plate):
    app.logger.info("TRACE::parse_german_plate")
    plate_size = len(plate)
    if plate_size == 6:
        match = re.search("^[A-Z]{1}[a-zA-Z0-9]{5}", plate)
        supposed_region = plate[0:1]
        app.logger.info("supposed_region: " + supposed_region)
        if match:
            app.logger.info("regex match")
            if supposed_region in germanRegions:
                return "DE"
            return None
        return None
    if plate_size == 7:
        match = re.search("^[A-Z]{2}[a-zA-Z0-9]{5}", plate)
        supposed_region = plate[0:2]
        if match:
            app.logger.info("regex match")
            if supposed_region in germanRegions:
                return "DE"
            return None
        return None
    if plate_size == 8:
        supposed_region = plate[0:3]
        match = re.search("^[A-Z]{3}[a-zA-Z0-9]{5}", plate)
        if match:
            app.logger.info("regex match")
            if supposed_region in germanRegions:
                return "DE"
            return None
        return None
    return None

'''
XX - one or two letters which indicate the local registration office 
(district where the registered possessor resides). As a general rule, 
State capitals have one letter; other districts have two letters.
heraldic emblem of the federal state the district belongs to (here shown as "∇"); 
diplomatic vehicles have a dash (–) instead, federal official vehicles wear the Austrian Federal Eagle.
A three to six-letter/number sequence which uniquely distinguishes 
each of the vehicles displaying the same initial area code. The letter Q is excluded from all sequences.
'''

# Issue of ambiguity, i.e. you could have a one or two letter 
# district and still have 3-6 alpha sequence

# TODO: The only way to fix this ambiguity is to check the one/two letter 
#       start sequence for a region match

# TODO: Use regions
def parse_austrian_plate(plate):
    match = re.search("^([A-Z]{1,2})([a-zA-Z0-9]{3,6})$", plate)
    if match:
        region = match.group(1)
        print("region: " + region)
        app.logger.info("regex match")
        return "AU"
    return None

'''
The numbering system goes as follows:

AA-001-AA to AA-999-AA (numbers evolve first);
AA-001-AB to AA-999-AZ (then the last letter on the right);
AA-001-BA to AA-999-ZZ (then the first letter on the right);
AB-001-AA to AZ-999-ZZ (then the last letter on the left);
BA-001-AA to ZZ-999-ZZ (then the first letter on the left).
'''

def parse_french_plate(plate):
    match = re.search("^[A-Z]{2}[0-9]{3}[A-Z]{2}", plate)
    if match:
        app.logger.info("regex match")
        return "FR"
    return None

'''
Plates issued between 1973 and 2008 used a combination of three letters 
followed by three numbers: "AAA-111". In June 2008 a new pattern was 
introduced with the numbers in front: "111-AAA". By this time the 1973 series 
had almost run out of combinations. Until the first quarter of 2009 the last 
plates of the old series (starting with Y, the Z series being reserved for dealer use) 
were issued simultaneously with the new layout, with the latter reserved at first 
for registrations made through Internet.

European-style number plates, introduced on 15 November 2010, have seven characters: 
a leading digit followed by three letters and three digits (1-AAA-111). 
Distribution of the 111-AAA series stopped at that moment, so that even 
though plates were printed in this series up to number 999-CFQ, 
the last ones have never been issued. This and all older series remain 
valid and no date is set for their expiration.

3 numbers 3 letters OR
1 number 3 letters 3 numbers
'''

def parse_belgian_plate(plate):
    match_2008 = re.search("^[0-9]{3}[A-Z]{3}", plate)
    match_2010 = re.search("^[0-9]{1}[A-Z]{3}[0-9]{3}", plate)
    if match_2008 or match_2010:
        app.logger.info("regex match")
        return "BE"
    return None

'''
The latest system of Czech vehicle registration plate was introduced between 
29 June and 17 July 2001. In this system, the first letter from the left 
represents the region (kraj), and this is combined with numbers issued 
in series from 1x0 0001, where x is the letter representing the region.

By 2009, Prague (A) has reached the combination 9x9 9999 in its respective series; 
consequently it then started issuing plates which included a two-letter combination 
in the format 1xa 0000 to 9xa 9999, where x is the regional letter and a is a letter 
in alphabetical order (so that 1AA 9999 is followed by 1AB 0000, and so on). 

Shortly after that the Central-Bohemian region came. As of the beginning of the 
summer 2014, the South Moravian (B) and Moravian-Silesian in the November, 
lastly in April 2019 the Usti region. Regions were also issuing registration 
marks with two-letter combinations. [1]
'''

def parse_czechia_plate(plate):
    match_first_series = re.search("^[1-9]{1}[A-Z]{1}[0-9]{5}", plate)
    match_second_series = re.search("^[1-9]{1}[A-Z]{2}[0-9]{5}", plate)
    if match_first_series or match_second_series:
        app.logger.info("regex match")
        return "CZ"
    return None

'''
Vehicle registration plates in Denmark normally have two letters and 
five digits and are issued by authorities. 
'''

def parse_danish_plate(plate):
    match = re.search("^[A-Z]{2}[0-9]{5}$", plate)
    if match:
        app.logger.info("regex match")
        return "DK"
    return None

'''
The standard series in use today uses a format of two letters followed by four digits (e.g., XY 3456)
'''
def parse_luxembourg_plate(plate):
    match = re.search("^[A-Z]{2}[0-9]{4}", plate)
    if match:
        app.logger.info("regex match")
        return "LU"
    return None

'''
https://en.wikipedia.org/wiki/Vehicle_registration_plates_of_the_Netherlands#:~:text=A%20plate%20bearing%20the%20number%201%20was%20issued,a%20province%20code%20and%20ddddd%20a%20serial%20number.
'''
def parse_dutch_plate(plate):
    # DE, DH, DL, DM, DR, AE, AH, AL, AM, AR and DZ are used for cars, ZM, ZF and NM for motorcycles and BE
    match_51_65 = re.search("^(DE|DH|DL|DM|DR|AE|AH|AL|AM|AR|DZ|ZM|ZF|NM|BE)[0-9]{4}$", plate)
    # ZZ
    match_65_73 = re.search("^[0-9]{4}(ZZ)$", plate)
    # This series is currently in use for imported "youngtimers" (1973 - 1977 vehicles), 
    # starting with Y (YA, YB, YD currently issued.)
    match_73_78 = re.search("^[0-9]{2}[A-Z]{2}[0-9]{2}$", plate)
    # Yellow plates started in this series. The letters K and Y were 
    # used as serial letters in this series.
    # This series is currently issued to semi-trailers (O)
    match_78_91 = re.search("^(K|Y){2}[0-9]{2}(K|Y){2}$", plate)
    match_91_99 = re.search("^(A-Z){4}[0-9]{2}$", plate)
    match_99_08 = re.search("^(0-9){2}[A-Z]{4}$", plate)

    match_06_1 = re.search("^[0-9]{2}[A-Z]{3}[0-9]{1}$", plate)

    # In this series, the first letters G, H, J, P, and R are not used, 
    # as to not clash with former export license plates.
    match_06_2 = re.search("^[0-9]{1}(?:(?![GHJPR])[A-Z]){3}[0-9]{2}$", plate)
    match_06_3 = re.search("^[A-Z]{2}[0-9]{3}[A-Z]{1}$", plate)

    # This series is currently issued for private vehicles, starting at G-001-BB.
    match_11 = re.search("^[G-Z]{1}[0-9]{3}[B-Z]{2}$", plate)
    match_15 = re.search("^[A-Z]{3}[0-9]{2}[A-Z]{1}$", plate)
    match_16 = re.search("^[0-9]{1}[A-Z]{2}[0-9]{3}$", plate)

    if match_51_65 or match_65_73 or match_73_78 or match_78_91 or match_91_99 \
        or match_99_08 or match_06_1 or match_06_2 or match_06_3 or match_11 \
        or match_15 or match_16:
        app.logger.info("regex match")
        return "NL"
    return None

'''
Format:

XY 12345
XY 1234J
XY 123JK
XY 1J345
XY 1JK45
XYZ J234
XYZ 12JK
XYZ 1J34 (first digit cannot be 0)
XYZ 12J4 (last digit cannot be 0)
XYZ 1JK4 (neither digit can be 0)
XYZ JK34
XYZ 12345
XYZ 1234J
XYZ 123JK
'''
def parse_polish_plates(plate):
    match_1 = re.search("^[A-Z]{2}[0-9]{5}$", plate)
    match_2 = re.search("^[A-Z]{2}[0-9]{4}[A-Z]{1}$", plate)
    match_3 = re.search("^[A-Z]{2}[0-9]{3}[A-Z]{2}$", plate)
    match_4 = re.search("^[A-Z]{2}[0-9]{1}[A-Z]{1}[0-9]{3}$", plate)
    match_5 = re.search("^[A-Z]{2}[0-9]{1}[A-Z]{2}[0-9]{2}$", plate)
    match_6 = re.search("^[A-Z]{4}[0-9]{3}$", plate)
    match_7 = re.search("^[A-Z]{3}[0-9]{2}[A-Z]{2}$", plate)
    match_8 = re.search("^[A-Z]{3}[1-9]{1}[A-Z]{1}[0-9]{2}$", plate) # first digit can't be 0
    match_9 = re.search("^[A-Z]{3}[0-9]{2}[A-Z]{1}[1-9]{1}$", plate) # last digit can't be 0
    match_10 = re.search("^[A-Z]{3}[1-9]{1}[A-Z]{2}[1-9]{1}$", plate) # neither digit can be 0
    match_11 = re.search("^[A-Z]{5}[0-9]{2}$", plate)
    match_12 = re.search("^[A-Z]{3}[0-9]{5}$", plate)
    match_13 = re.search("^[A-Z]{3}[0-9]{4}[A-Z]{1}$", plate)
    match_14 = re.search("^[A-Z]{3}[0-9]{3}[A-Z]{2}$", plate)

    if match_1 or match_2 or match_3 or match_4 or match_5 or match_6 or \
       match_7 or match_8 or match_9 or match_10 or match_11 or match_12 or \
       match_13 or match_14:
        app.logger.info("regex match")
        return "PL"
    return None


'''
Vehicle license plates of Switzerland, are composed of a two-letter code 
for the canton and a number with up to 6 digits.
'''
def parse_swiss_plate(plate):
    match = re.search("^[A-Z]{2}[0-9]{6}", plate)
    if match:
        app.logger.info("regex match")
        return "CH"
    return None

# Neighbouring countries of Germany
# 1Austria(done)
# 2Belgium(done)
# 3Czech Republic(done)
# 4Denmark(done)
# 5France(done)
# 6Luxembourg(done)
# 7Netherlands(done)
# 8Poland(done)
# 9Switzerland(done)

# general parsing
def parse(plate):
    countries = []
    if parse_german_plate(plate):
        countries.append("DE")
    if parse_austrian_plate(plate):
        countries.append("AU")
    if parse_french_plate(plate):
        countries.append("FR")
    if parse_belgian_plate(plate):
        countries.append("BE")
    if parse_czechia_plate(plate):
        countries.append("CZ")
    if parse_danish_plate(plate):
        countries.append("DK")
    if parse_luxembourg_plate(plate):
        countries.append("LU")
    if parse_dutch_plate(plate):
        countries.append("NL")
    if parse_polish_plates(plate):
        countries.append("PL")
    if parse_swiss_plate(plate):
        countries.append("CH")
    return countries