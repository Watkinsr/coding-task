from app import db
from datetime import datetime

class Plates(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    plate = db.Column(db.String, nullable=False)
    timestamp = db.Column(db.Integer, nullable=False, default=datetime.utcnow)
    country = db.Column(db.String, nullable=False)

    def __init__(self, plate, timestamp, country):
        self.plate = plate
        self.timestamp = timestamp
        self.country = country

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'plate'         : self.plate,
           'timestamp'     : self.timestamp,
           'country'       : self.country
       }