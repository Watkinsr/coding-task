# README

I am using VSCode/emacs for this project and a virtualenv. The project parses number plate data as an endpoint.

It would be good to have SQLite installed as the plates.db in the repo is a SQLite database.

## Development Setup
```console
virtualenv .venv
source .venv/bin/activate
pip install flask sqlalchemy flask-sqlalchemy epc importmagic
```
