from app import app, db
from flask import jsonify, request, Response
from models import Plates
from datetime import datetime
from parse import parse

import json

@app.route('/plate', methods=['GET', 'POST'])
def plate():
    if request.method == 'POST':
        if request.is_json:
            content = request.get_json()
            if (content['plate']):
                plate = content['plate']
                countries = parse(plate)
                if countries:
                    pl = Plates(plate, datetime.utcnow(), ','.join(countries))
                    db.session.add(pl)
                    db.session.commit()
                    country_json = json.dumps({
                        'country': countries
                    })
                    return Response(country_json, status=200, mimetype='application/json')
                app.logger.info("Unable to parse for regions")
                return Response(status=422) # Couldn't parse for our regions
            return Response(status=400)
        return Response(status=400)
    plates = [i.serialize for i in Plates.query.all()]
    for plate in plates:
        plate['country'] = plate['country'].split(',')
    return jsonify(plates)
